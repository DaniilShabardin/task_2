#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRegularExpression>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->Telefon,&QLineEdit::textEdited,[this](QString text){
        QRegularExpression exp("\\+(\\d{11})$");
        if(exp.match(text).hasMatch())OK();
        else FAIL();
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OK()
{
    ui->Otvet->setText("OK");
    ui->Otvet->setStyleSheet("qproperty-alignment: 'AlignVCenter | AlignHCenter'; background-color: rgb(255, 255, 255); color: rgb(0, 255, 0);");
}

void MainWindow::FAIL()
{
    ui->Otvet->setText("FAIL");
    ui->Otvet->setStyleSheet("qproperty-alignment: 'AlignVCenter | AlignHCenter'; background-color: rgb(255, 255, 255); color: rgb(255, 0, 0);");
}
